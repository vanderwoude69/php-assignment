-- --------------------------------------------------------
-- Host:                         localhost
-- Server versie:                5.7.22 - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Databasestructuur van assignment wordt geschreven
DROP DATABASE IF EXISTS `assignment`;
CREATE DATABASE IF NOT EXISTS `assignment` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `assignment`;

-- Structuur van  tabel assignment.accus wordt geschreven
DROP TABLE IF EXISTS `accus`;
CREATE TABLE IF NOT EXISTS `accus` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) DEFAULT NULL,
    `price` decimal(20,2) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel assignment.accus: ~4 rows (ongeveer)
/*!40000 ALTER TABLE `accus` DISABLE KEYS */;
INSERT INTO `accus` (`id`, `name`, `price`) VALUES
                                                (1, 'Universele accu voor ATB, MTB', 269.00),
                                                (2, 'Fietsaccu voor Matrabike - E4-Motion - Prolithium', 229.00),
                                                (3, 'Fietsaccu ANWB XT10 - 9,0 Ah of 11,0 AH', 299.00),
                                                (4, 'Originele Prophete Samsung accu Li-ION 13,7 Ah ', 449.00);
/*!40000 ALTER TABLE `accus` ENABLE KEYS */;

-- Structuur van  tabel assignment.bikes wordt geschreven
DROP TABLE IF EXISTS `bikes`;
CREATE TABLE IF NOT EXISTS `bikes` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL DEFAULT '',
    `supplier_id` int(10) NOT NULL DEFAULT '0',
    `accu_id` int(11) DEFAULT NULL,
    `price` float(11,2) DEFAULT NULL,
    `type` enum('male','female') DEFAULT NULL,
    `image` varchar(50) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `supplier_id` (`supplier_id`),
    KEY `batteries_id` (`accu_id`) USING BTREE
    ) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel assignment.bikes: ~8 rows (ongeveer)
/*!40000 ALTER TABLE `bikes` DISABLE KEYS */;
INSERT INTO `bikes` (`id`, `name`, `supplier_id`, `accu_id`, `price`, `type`, `image`) VALUES
                                                                                           (5, 'Rustiek', 3, 3, 1995.60, 'female', '2020_cutout_prem.png'),
                                                                                           (6, 'Speedy', 2, 3, 1631.00, 'male', 'dames-elektrische-fiets-kalkhof.jpg'),
                                                                                           (7, 'Sloompie', 3, 3, 10.00, 'female', '2020_cutout_prem.png'),
                                                                                           (14, 'Roadrunner', 4, 1, 19.63, 'male', 'Cannondale-Mavaro-Neo-E-bike.jpg'),
                                                                                           (15, 'Rapid runner', 6, 3, 1022.30, 'female', 'dames-elektrische-fiets-kalkhof.jpg');
/*!40000 ALTER TABLE `bikes` ENABLE KEYS */;

-- Structuur van  tabel assignment.bike_parts wordt geschreven
DROP TABLE IF EXISTS `bike_parts`;
CREATE TABLE IF NOT EXISTS `bike_parts` (
    `bikes_id` int(10) DEFAULT NULL,
    `parts_id` int(10) DEFAULT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumpen data van tabel assignment.bike_parts: ~31 rows (ongeveer)
/*!40000 ALTER TABLE `bike_parts` DISABLE KEYS */;
INSERT INTO `bike_parts` (`bikes_id`, `parts_id`) VALUES
                                                      (7, 10),
                                                      (7, 11),
                                                      (7, 12),
                                                      (14, 8),
                                                      (14, 9),
                                                      (14, 10),
                                                      (14, 11),
                                                      (14, 12),
                                                      (15, 2),
                                                      (15, 3),
                                                      (15, 4),
                                                      (15, 5),
                                                      (5, 1),
                                                      (5, 2),
                                                      (5, 3),
                                                      (5, 4),
                                                      (5, 5),
                                                      (6, 1),
                                                      (6, 2),
                                                      (6, 3),
                                                      (6, 4);
/*!40000 ALTER TABLE `bike_parts` ENABLE KEYS */;

-- Structuur van  tabel assignment.parts wordt geschreven
DROP TABLE IF EXISTS `parts`;
CREATE TABLE IF NOT EXISTS `parts` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) DEFAULT NULL,
    `price` decimal(20,2) DEFAULT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel assignment.parts: ~12 rows (ongeveer)
/*!40000 ALTER TABLE `parts` DISABLE KEYS */;
INSERT INTO `parts` (`id`, `name`, `price`) VALUES
                                                (1, 'Bafang DP-C07 CANBUS display', 129.96),
                                                (2, 'Bafang M420 tandwiel 38T', 5.95),
                                                (3, 'E-Drive P-01 controller (UART) 36V 15A', 119.95),
                                                (4, 'E-Drive Regelaar 36v 13a led 6-standen display', 59.95),
                                                (5, 'Ebike kabelhouder voor twee kabels', 1.49),
                                                (6, 'Spiraalband zwart', 2.50),
                                                (7, 'Axa koplamp Blueline 30', 24.95),
                                                (8, 'Koplamp Gazelle FenderLight V/2 E-bike', 64.95),
                                                (9, 'EBP ACHTERLICHT SPAN ION PMU4 Spanninga', 15.25),
                                                (10, 'AXA Koplamp Pico 30-E Switch 6-42 Volt', 27.95),
                                                (11, 'Duimgas', 14.95),
                                                (12, 'Handgas Full twist', 14.95);
/*!40000 ALTER TABLE `parts` ENABLE KEYS */;

-- Structuur van  tabel assignment.supplier wordt geschreven
DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
    `id` int(10) NOT NULL AUTO_INCREMENT,
    `name` varchar(50) NOT NULL,
    PRIMARY KEY (`id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumpen data van tabel assignment.supplier: ~6 rows (ongeveer)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` (`id`, `name`) VALUES
                                          (1, 'Stella'),
                                          (2, 'Batavus'),
                                          (3, 'Giant'),
                                          (4, 'Cube'),
                                          (5, 'Trek'),
                                          (6, 'Gazelle');
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
