<?php

namespace Src\Actions;

use Src\Classes\FileUpload;

class UploadImageFile
{
    protected  $errors = [];

    public function __invoke(array $files, string $exsisting_image, bool $allowToDeleteImage)
    {
        $fileUpload = new FileUpload($files);

        $fileUpload->setExsistinImage($exsisting_image);
        $fileUpload->setAllowedImageTypes('image/jpeg');
        $fileUpload->setAllowedImageTypes('image/png');
        $fileUpload->setAllowedImageSize(200000);
        $fileUpload->setAllowToDeleteImage($allowToDeleteImage);

        if (!$fileUpload->checkImageType()) {
            $this->errors[] = "De bestandsformaat is niet goed het mogen alleen .png en .jpg zijn.";
        }
        if (!$fileUpload->checkImageSize()) {
            $this->errors[] = "De afbeelding is te groot om opgeslagen te worden. De maximale grote mag $fileUpload->allowedImageSize bytes zijn";
        }

        if(count($this->errors) === 0){
            if($fileUpload->upload()){
                return $fileUpload->imageName();
            }
        }else{
            return $this->errors;
        }
    }
}