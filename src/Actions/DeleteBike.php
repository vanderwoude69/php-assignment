<?php

namespace Src\Actions;

use Src\Models\Bike;
use Src\Models\BikeParts;

class DeleteBike
{
    public function __invoke($bike_id)
    {
        $bikes = new Bike();
        $bike = $bikes->show($bike_id);
        try {
            if ($bike) {

                $bikes->setImage($bike->image);

                if($bikes->allowedToDeleteImage()) {
                    unlink('images/' . $bike->image);
                }
                $bikes->delete();
                (new BikeParts())->deletePartsByBikeId($bike_id);
            }
        } catch (\Exception $e) {
            $this->showError("Kan de fiets niet ophalen", $e->getMessage());
        }
    }
}