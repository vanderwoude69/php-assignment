<?php

namespace Src\Classes;

use function PHPUnit\Framework\returnArgument;

class FileUpload
{
    public $targegtDir = 'images/';

    public $imageName;
    public $imageType;
    public $imageSize;
    public $imageTempName;

    public $exsistinImage;

    public $allowedImageTypes = [];
    public $allowedImageSize;
    public $allowToDelete;

    public function __construct(array $file)
    {
        $this->imageName = $file['image']['name'];
        $this->imageType = $file['image']['type'];
        $this->imageSize = $file['image']['size'];
        $this->imageTempName = $file['image']['tmp_name'];
    }

    public function setAllowedImageTypes(string $allowedTypes)
    {
        $this->allowedImageTypes[] = $allowedTypes;
    }

    public function setAllowedImageSize(int $allowedSize)
    {
        $this->allowedImageSize = $allowedSize;
    }

    public function checkImageType()
    {
        if (in_array($this->imageType, $this->allowedImageTypes)) {
            return true;
        }
        return false;
    }

    public function checkImageSize()
    {
        if ($this->imageSize <= $this->allowedImageSize) {
            return true;
        }
        return false;
    }

    public function getAllowedImageTypes(): array
    {
        return $this->allowedImageTypes;
    }

    public function getAllowedImageSize(): int
    {
        return $this->allowedImageSize;
    }

    public function getExsistinImage(): string
    {
        return $this->exsistinImage;
    }

    public function setExsistinImage($exsistinImage): void
    {
        $this->exsistinImage = $exsistinImage;
    }

    public function imageName(): string
    {
        return $this->imageName;
    }

    public function setAllowToDeleteImage(bool $allowToDelete)
    {
        $this->allowToDelete = $allowToDelete;
    }

    public function upload()
    {
        if($this->exsistinImage !== '' && file_exists($this->targegtDir . $this->exsistinImage)) {
            if($this->allowToDelete === true){
                unlink($this->targegtDir . $this->exsistinImage);
            }
        }
        if (move_uploaded_file($this->imageTempName, $this->targegtDir . $this->imageName)) {
            return true;
        } else {
            return false;
        }
    }
}