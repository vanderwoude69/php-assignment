<?php

namespace Src\Classes;

use Exception;

class Dispatcher
{
    public function handleRequest(array $request)
    {

        if(isset($request['category'])) {
            $category = ucfirst($request['category']);
            $controller = "Src\\Controllers\\{$category}Controller";
        } else {
            $controller = "Src\\Controllers\\EbikeController"; // default
        }

        $controller = new $controller();

        $op = isset($request['op']) ? $request['op'] : null;

        try {
            if (!$op || $op == 'list') {
                $controller->list(isset($request['supplier_id']) ? $request['supplier_id'] : 0);
            } elseif ($op == 'search'){

                $controller->search($request['search']);
            } elseif ($op == 'add') {
                $controller->add();
            } elseif ($op == 'save') {
                $controller->save();
            } elseif (isset($request['id']) && ($op == 'delete' || $op == 'edit')) {
                if ($op == 'delete') {
                    $controller->delete($request['id']);
                } else {
                    $controller->show($request['id']);
                }
            } else {
                $controller->showError("Page not found", "Pagina " . $op . " is niet gevonden!");
            }
        } catch (Exception $e) {
            $controller->showError("Applicatie fout", $e->getMessage());
        }
    }
}