<?php

namespace Src\Classes;

use Src\Models\TestInterface;

class test
{

    private TestInterface $test;

    public function __construct(TestInterface $test)
    {
        $this->test = $test;
    }

    public function name(){
       echo $this->test->callName();
    }
}