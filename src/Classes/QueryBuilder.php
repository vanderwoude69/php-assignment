<?php

namespace Src\Classes;

use PDO;
use Src\Classes\Actions\WhereToString;
use Src\Connect\database;

class QueryBuilder
{
    protected $connect;
    
    protected string $sql;
    
    protected function connect()
    {
        $db = new database();
        return $db->dbConnection();
    }
    
    public function getObject(string $table, int $id): object
    {
        $sql = "SELECT * FROM $table WHERE id = $id";
    
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        return $stmt->fetchObject();
    }
    
    public function find(array $criteria = [], string $join = '', array $columns = [])
    {
        $whereToString = new WhereToString();
        $where = count($criteria) !== 0 ? ($whereToString)($criteria) : ' ' ;
        
        if (count($columns) > 0 ){
            $columns = implode(',', $columns);
        }else{
            $columns = '*';
        }
        
        $sql = trim("SELECT $columns FROM $this->table $join $where");
    
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        return $stmt->fetchAll();
    }
    
    public function do_delete(int $id)
    {
        $sql = "DELETE FROM $this->table WHERE id = $id";
        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
    }
}