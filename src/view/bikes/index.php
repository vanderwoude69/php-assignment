<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bikes</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Ruuds e-bikestore</a>
        </div>
        <div class="collapse navbar-collapse col-md-3" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/?category=ebike&op=list">Elektrische fietsen<span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
        <div class="collapse navbar-collapse col-md-3" id="bs-example-navbar-collapse-1" style="margin-top:8px;">
            <form methode="GET" class="form-inline" action="/?category=ebike&op=search">
                <input type="hidden" name="category" value="ebike">
                <input type="hidden" name="op" value="search">
                <input class="form-control" type="search" name="search" placeholder="Zoeken" aria-label="Search" required>
                <button class="btn btn-outline-success" type="submit">Zoeken</button>
            </form>
        </div>
    </div>
</nav>
    <div class="container row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <?php foreach ($suppliers as $supplier): ?>
                    <li><a href="/?category=ebike&op=list&supplier_id=<?= $supplier['id'] ?>"><?= $supplier['name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-10">
            <div class="col-md-12" style="margin-bottom: 10px">
                <div class="pull-left">
                    <?php if ($search): ?>
                        <?php if(count($bikes) === 1): ?>
                            Er is <?= count($bikes) ?> fiets gevonden met de zoekterm "<?= $search ?>".
                        <?php else: ?>
                            Er zijn <?= count($bikes) ?> fietsen gevonden met de zoekterm "<?= $search ?>".
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="pull-right">
                    <a href="/?category=ebike&op=add" class="btn btn-primary">Voeg fiets toe</a>
                </div>
            </div>

            <div class="col-md-12">
                <?php if(count($bikes) === 0): ?>
                    <div style="margin-top: 200px border:1px solid gray; padding:25px" class="text-center">
                        <h3>Er zijn geen fietsen gevonden</h3>
                    </div>
                <?php else: ?>

                <table class="table table-bordered">
                    <tr>
                        <th style="width:100px"></th>
                        <th>Fiets</th>
                        <th>Leverancier</th>
                        <th>type</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php foreach($bikes as $bike): ?>
                    <tr>
                        <td>
                            <?php if ($bike['image'] !== null): ?>
                            <img src="images/<?= $bike['image'] ?>" class="img-responsive">
                            <?php endif; ?>
                        </td>
                        <td><?= $bike['name'] ?></td>
                        <td><?= $bike['supplier'] ?></td>
                        <td>
                            <?php if($bike['type'] === 'male'): ?>Heren<?php endif; ?>
                                    <?php if($bike['type'] === 'female'): ?>Dames<?php endif; ?>
                        </td>
                        <td><a href="/?category=ebike&op=edit&id=<?= $bike['id'] ?>" class="btn btn-default">Wijzig</a></td>
                        <td><button type="button" data-toggle="modal" data-target="#deleteBike" class="btn btn-danger" data-id="<?= $bike['id'] ?>" data-name="<?= $bike['name'] ?>">Verwijder</button></td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>
            <?php endif; ?>

        </div>
    </div>

    <div class="modal fade" id="deleteBike" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Verwijder fiets</h4>
                </div>
                <div class="modal-body">
                    Weet u zeker dat u de <span id="bikeName"></span> fiets wilt verwijderen.
                </div>
                <div class="modal-footer">
                    <form method="GET" action="/?category=ebike&op=delete">
                        <input type="hidden" name="category" value="ebike">
                        <input type="hidden" name="op" value="delete">
                        <input type="hidden" name="id" id="bikeId">
                        <button type="submit" class="btn btn-danger">Verwijder</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        $('#deleteBike').on('show.bs.modal', function (e) {
            var bikeId = $(e.relatedTarget).attr('data-id');
            $(this).find('#bikeId').val(bikeId);

            var bikeName = $(e.relatedTarget).attr('data-name');
            $(this).find('#bikeName').text(bikeName);
        });
    </script>
</body>
</html>

