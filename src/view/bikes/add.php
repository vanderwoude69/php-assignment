<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Bikes</title>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Ruuds e-bikestore</a>
        </div>
        <div class="collapse navbar-collapse col-md-3" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="/?category=ebike&op=list">Elektrische fietsen<span class="sr-only">(current)</span></a></li>
            </ul>
        </div>
        <div class="collapse navbar-collapse col-md-3" id="bs-example-navbar-collapse-1" style="margin-top:8px;">
            <form methode="GET" class="form-inline" action="/?category=ebike&op=search">
                <input type="hidden" name="category" value="ebike">
                <input type="hidden" name="op" value="search">
                <input class="form-control" type="search" name="search" placeholder="Zoeken" aria-label="Search" required>
                <button class="btn btn-outline-success" type="submit">Zoeken</button>
            </form>
        </div>
    </div>
</nav>

<div class="container row">
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">
                <?php foreach ($suppliers as $supplier): ?>
                    <li><a href="/?category=ebike&op=list&supplier_id=<?= $supplier['id'] ?>"><?= $supplier['name'] ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <h3>
                        <?php if($bike !== null){ ?>
                            Wijzig Elektrische fiets <?= $bike->name ?>:
                        <?php } else { ?>
                            Voeg Elektrische fiets toe:
                        <?php } ?>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p>velden met een <span style="color:red">*</span> zijn verplicht</p>
                    <form method="post" action="?category=ebike&op=save" enctype="multipart/form-data">
                        <?php if($bike !== null){ ?>
                            <input type="hidden" name="id" value="<?= $bike->id ?>">
                        <?php } ?>
                        <div class="form-group">
                            <label for="bike">Fiets <span style="color:red">*</span></label>
                            <input type="text" name="bike" class="form-control" id="bike" placeholder="" value="<?= $bike->name ?>" required>
                        </div>
                        <div class="form-group">

                            <input type="hidden" name="exsisting_image" value="<?= $bike->image ?>"
                            <label for="image">Afbeelding</label>
                            <input type="file" name="image" class="form-control" id="image" placeholder="">
                            <?php if(count($errors) !== 0): ?>
                                <div class="alert alert-danger" role="alert">
                                    <ul>
                                        <?php foreach ($errors as $error): ?>
                                            <li><?= $error ?></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="form-group">
                            <label for="price">Prijs <span style="color:red">*</span></label>
                            <input type="text" name="price" class="form-control" id="price" placeholder="" value="<?= $bike->price ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="type">Type <span style="color:red">*</span></label>
                            <select name="type" id="type" class="form-control" required>
                                <option value="0"></option>
                                <option value="female" <?php if($bike->type === 'female'){ ?> selected="selected"<?php } ?>>Dames</option>
                                <option value="male" <?php if($bike->type === 'male'){ ?> selected="selected"<?php } ?>>Heren</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="accu">Accu <span style="color:red">*</span></label>
                            <select name="accu_id" id="accu" class="form-control" required>
                                <option value="">Voeg accu toe</option>
                                <?php foreach($accus as $accu): ?>
                                    <option value="<?= $accu['id'] ?>" <?php if($bike->accu_id === $accu['id']){ ?> selected="selected"<?php } ?>><?= $accu['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="supplier">Leverancier <span style="color:red">*</span></label>
                            <select name="supplier_id" id="supplier" class="form-control" required>
                                <option value="">Voeg leverancier toe</option>
                                <?php foreach($suppliers as $supplier): ?>
                                    <option value="<?= $supplier['id'] ?>" <?php if($bike->supplier_id === $supplier['id']){ ?> selected="selected"<?php } ?>><?= $supplier['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="part">Onderdelen</label>
                            <select name="part_ids[]" id="part" class="form-control" multiple size="6">
                                <option value="0">Voeg onderdeel toe</option>
                                <?php foreach($parts as $part): ?>
                                    <option value="<?= $part['id'] ?>" <?php if($selectedParts !== null && in_array($part['id'], $selectedParts)){ ?> selected="selected"<?php } ?>><?= $part['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">Submit</button>

                        <?php if($bike !== null){ ?>
                            <button type="button" data-toggle="modal" data-target="#deleteBike" class="btn btn-danger" data-id="<?= $bike->id ?>" data-name="<?= $bike->name ?>">Verwijder</button>
                        <?php } ?>
                    </form>
                </div>

                <div class="col-md-4">
                    <?php if ($bike->image !== null): ?>
                        <img src="images/<?= $bike->image ?>" class="img-responsive">
                    <?php else: ?>
                        <div class="text-center">Geen afbeelding beschikbaar</div>
                    <?php endif; ?>
                </div>
            </div>


        </div>
 </div>

<div class="modal fade" id="deleteBike" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Verwijder fiets</h4>
            </div>
            <div class="modal-body">
                Weet u zeker dat u de <span id="bikeName"></span> fiets wilt verwijderen.
            </div>
            <div class="modal-footer">
                <form method="GET" action="/?op=delete">
                    <input type="hidden" name="category" value="ebike">
                    <input type="hidden" name="op" value="delete">
                    <input type="hidden" name="id" id="bikeId">
                    <button type="submit" class="btn btn-danger">Verwijder</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Annuleren</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $('#deleteBike').on('show.bs.modal', function (e) {
        var bikeId = $(e.relatedTarget).attr('data-id');
        $(this).find('#bikeId').val(bikeId);

        var bikeName = $(e.relatedTarget).attr('data-name');
        $(this).find('#bikeName').text(bikeName);
    });
</script>

</body>
</html>