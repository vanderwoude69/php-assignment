<?php

namespace Src\Models;

use Src\Classes\QueryBuilder;

class Supplier extends QueryBuilder
{
    protected string $table = 'supplier';
    
    protected $connect;

    protected $id;
    protected $name;
    protected $price;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    public function all()
    {
        return  $this->find();
    }
}