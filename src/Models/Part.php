<?php

namespace Src\Models;

use PDO;
use Src\Classes\QueryBuilder;
use Src\Connect\database;

class Part extends QueryBuilder
{
    protected string $table = 'parts';
    
    protected $id;
    protected $name;
    protected $price;

    protected $parts;
    protected $bike_id;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    public function all()
    {
        return $this->find();
        
    }
}