<?php

namespace Src\Models;

use PDO;
use Src\Classes\QueryBuilder;
use Src\Connect\database;

class BikeParts extends QueryBuilder
{
    protected string $table = 'bike_parts';

    public function connect()
    {
        $db = new database();
        return $db->dbConnection();
    }

    public function setParts(array $parts)
    {
        $this->parts = $parts;
    }

    public function setBikeId(int $bike_id)
    {
        $this->bike_id = $bike_id;
    }

    public function save()
    {
        if ($this->getById($this->bike_id)) {
            $this->deletePartsByBikeId($this->bike_id);
        }

        $sql = "INSERT INTO bike_parts (bikes_id, parts_id) values (?, ?)";

        foreach($this->parts as $part){
            $stmt = $this->connect()->prepare($sql);
            $stmt->execute([$this->bike_id, $part]);
        }
    }

    public function deletePartsByBikeId(int $bike_id)
    {
        $this->do_delete($bike_id);
    }

    public function getById(int $bike_id): array
    {
        return $this->find(['bike_id'=>$bike_id]);
    }

    public function getPartsIdByBikeId(int $bike_id): array
    {
        $partIds = [];
        foreach($this->getById($bike_id) as $part) {
            $partIds[] = $part['parts_id'];
        }
        return $partIds;
    }
}