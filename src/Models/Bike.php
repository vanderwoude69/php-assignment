<?php

namespace Src\Models;

use PDO;
use Src\Classes\QueryBuilder;

class Bike extends QueryBuilder
{
    protected string $table = 'bikes';
    
    public $id = null;
    public $name;
    public $supplier_id;
    public $accu_id;
    public $price;
    public $type;
    public $parts;
    public $image = '';
    
    
    /**
     * @return mixed
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSupplierId(): int
    {
        return $this->supplier_id;
    }

    /**
     * @param mixed $supplier_id
     */
    public function setSupplierId(int $supplier_id): void
    {
        $this->supplier_id = $supplier_id;
    }

    /**
     * @return mixed
     */
    public function getAccuId(): int
    {
        return $this->accu_id;
    }

    /**
     * @param mixed $batteries_id
     */
    public function setAccuId(int $accu_id): void
    {
        $this->accu_id = $accu_id;
    }

    /**
     * @return mixed
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice(string $price): void
    {
        $this->price = floatval($price);
    }

    /**
     * @return mixed
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function save(): int
    {
        $conn = new PDO('mysql:host=db-php-assignment; dbname=assignment', 'development', 'development');

        if($this->id === null) {
            $sql = "INSERT INTO bikes (`name`, supplier_id, accu_id, price, `type`, image) values (?, ?, ?, ?, ?, ?)";
        } else {
            $sql = "UPDATE bikes SET `name` = ?, supplier_id = ?, accu_id = ?, price = ?, `type` = ?, image = ? WHERE id = $this->id";
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute([$this->name, $this->supplier_id, $this->accu_id, $this->price, $this->type, $this->image]);

        $lastInsertedID = $conn->lastInsertId() !== "0" ? $conn->lastInsertId() : $this->id ;

        if($lastInsertedID && $this->parts !== NULL) {
            $bikeParts = new BikeParts();
            $bikeParts->setParts($this->parts);
            $bikeParts->setBikeId($lastInsertedID);
            $bikeParts->save();
        }
        return $lastInsertedID;
    }

    public function read(int $supplierId = 0): array
    {
        $columns = ['bikes.*', 'supplier.name as supplier '];
        $join = ' LEFT JOIN supplier ON bikes.supplier_id = supplier.id ';
        
        if($supplierId === 0){
            return $this->find([], $join, $columns);
        }else{
            return $this->find(['supplier_id' => $supplierId], $join, $columns);
        }
    }

    public function search(string $search): array
    {
        $sql = "SELECT bikes.*, supplier.name as supplier FROM bikes 
            LEFT JOIN supplier ON bikes.supplier_id = supplier.id 
            WHERE (bikes.name LIKE '%$search%'
            OR supplier.name LIKE '%$search%')";

        $stmt = $this->connect()->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        return $stmt->fetchall();
    }

    public function get(int $id)
    {
        $this->setId($id);
        return $this->getObject('bikes', $id);
    }

    public function delete()
    {
        $this->do_delete($this->id);
    }

    public function allowedToDeleteImage(): bool
    {
        if($this->image === null){
            return false;
        }

        if (count($this->find(['image'=>$this->image])) === 1) {
            return true;
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getParts(): array
    {
        return $this->parts;
    }

    /**
     * @param mixed $parts
     */
    public function setParts(array $parts): void
    {
        $this->parts = $parts;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = $image;
    }


}