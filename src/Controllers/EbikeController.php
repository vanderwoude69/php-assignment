<?php

namespace Src\Controllers;

use Src\Actions\DeleteBike;
use Src\Actions\UploadImageFile;
use Src\Models\Bike;
use Src\Models\Accu;
use Src\Models\BikeParts;
use Src\Models\Part;
use Src\Models\Supplier;

class EbikeController
{

    protected $bikes = null;
    protected $errors = [];

    public function __construct()
    {
        $this->bikes = new Bike();
    }

    public function redirect($location)
    {
        header('Location: ' . $location);
    }

    public function showError($title, $message)
    {
        include 'src/view/errorpage.php';
    }

    public function list(int $supplierId): void
    {
        $bikes = $this->bikes->read($supplierId);
        $suppliers = (new Supplier())->all();
        include 'src/view/bikes/index.php';
    }

    public function search(string $search): void
    {
        $bikes = $this->bikes->search(htmlspecialchars($search));
        $suppliers = (new Supplier())->all();
        include 'src/view/bikes/index.php';
    }

    public function add(): void
    {
        $accus = (new Accu())->all();
        $suppliers = (new Supplier())->all();
        $parts = (new Part())->all();
        $errors = [];

        include 'src/view/bikes/add.php';
    }

    public function save()
    {
        $bike = $this->bikes;

        if (isset($_POST['id'])) {
            $bike->setId($_POST['id']);
        }
        $bike->setImage($_POST['exsisting_image']);
        if ($_FILES['image']['name'] !== '') {

            $uploadImageFile = new UploadImageFile();
            $uploadedImage = ($uploadImageFile)($_FILES, $_POST['exsisting_image'], $bike->allowedToDeleteImage());

            if(!is_array($uploadedImage)){
                $bike->setImage($uploadedImage);
            } else {
                $this->errors = $uploadedImage;
            }
        }

        $bike->setName(htmlspecialchars($_POST['bike']));
        $bike->setPrice(htmlspecialchars($_POST['price']));
        $bike->setType($_POST['type']);
        $bike->setAccuId($_POST['accu_id']);
        $bike->setSupplierId($_POST['supplier_id']);

        if(isset($_POST['part_ids']) && count($_POST['part_ids']) !== 0){
            $bike->setParts($_POST['part_ids']);
        }
        $id = $bike->save();

        $this->show($id, $this->errors);
    }

    public function show(int $id, array $errors = [])
    {
        $bike = $this->bikes->get($id);
        if ($bike !== null) {
            $selectedParts = (new BikeParts())->getPartsIdByBikeId($bike->id);
        }

        $suppliers = (new Supplier())->all();
        $parts = (new Part())->all();
        $accus = (new Accu())->all();
        $errors = $this->errors;

        include 'src/view/bikes/add.php';
    }

    public function delete(int $bike_id)
    {
        $deleteBike = new DeleteBike();
        ($deleteBike)($bike_id);

        $this->redirect('?category=ebike&op=list');
    }

}