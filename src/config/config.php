<?php
//site name
define('SITE_NAME', 'Biky');

//App Root
define('APP_ROOT', dirname(dirname(__FILE__)));
define('URL_ROOT', '/');
define('URL_SUBFOLDER', '');

//DB Params
define('DB_HOST', 'db-php-assignment');
define('DB_USER', 'development');
define('DB_PASS', 'development');
define('DB_NAME', 'assignment');
