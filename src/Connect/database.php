<?php

namespace Src\Connect;

use PDO;
use PDOException;

class database
{

    public $connect = null;

    public function dbConnection()
    {
        if($this->connect !== null) {
            return $this->connect;
        }

        try{
            $this->connect = new PDO('mysql:host=db-php-assignment; dbname=assignment', 'development', 'development');
        }catch(PDOException $exception) {
            exit('Database failed to connect: ' . $exception);
        }
        return $this->connect;
    }
}