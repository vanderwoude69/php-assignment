<?php
require 'vendor/autoload.php';

require_once 'src/config/config.php';

$controller = new \Src\Classes\Dispatcher();
$controller->handleRequest($_REQUEST);
